<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/united/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		  <a class="navbar-brand" href="../index.php">Pizzeria Bianco</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor01">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="catalog.php">Menu <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="add-item.php">Add item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="cart.php">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
	</nav>

	<!-- Page Contents -->
	<?php get_body_contents()


	?>



	<!-- Footer -->
	<footer class="page-footer font-small bg-primary text-light">
		<div class="footer-copyright text-center py3">
			2020 Made by Onalyn Go
		</div>
	</footer>
</body>
</html>